import java.util.ArrayList;

import org.w3c.dom.Document;

import br.com.github.boveloco.Parse_XML.Domain.Pessoa;
import br.com.github.boveloco.Parse_XML.Services.HandleXML;
import br.com.github.boveloco.Parse_XML.View.HandleHTML;


public class Main {

	public static void main(String[] args) {
		HandleXML xml = new HandleXML();
		HandleHTML html = new HandleHTML();
		
		Document f_Xml = xml.Import();
		System.out.println("Importou xml corretamente");
		ArrayList<Pessoa> arrayPessoas = xml.getPessoas(f_Xml);
		System.out.println("Importou as pessoas corretamente");
		String s_Html = xml.ListDetailPersona(arrayPessoas);
		System.out.println("Gerou HTML pessoas COrretamente");
		html.writeHtml(s_Html);
		System.out.println("Escreveu o arquivo corretamente");
	}

}
