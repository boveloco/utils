package br.com.github.boveloco.Parse_XML.Domain;

public class Pessoa {

	private String ID;
	
	private String Name;
	
	private String Description;
	
	private String LinkImage;

	//gss
	
	public String getID() {
		return ID;
	}

	public void setID(String iD) {
		ID = iD;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public String getDescription() {
		return Description;
	}

	public void setDescription(String description) {
		Description = description;
	}

	public String getLinkImage() {
		return LinkImage;
	}

	public void setLinkImage(String linkImage) {
		LinkImage = linkImage;
	}
	
}
