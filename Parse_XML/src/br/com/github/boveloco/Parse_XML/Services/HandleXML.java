package br.com.github.boveloco.Parse_XML.Services;

import java.io.File;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import br.com.github.boveloco.Parse_XML.Domain.Pessoa;

public class HandleXML {

	public Document Import(){
		try{
			//importa o xml.
			File f_xml = new File("src/personalidades.xml");
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(f_xml);
			
			doc.getDocumentElement().normalize();
			
			return doc;
		} catch(Exception e){
			e.printStackTrace();
			return null;
		}		
	}
	
	public ArrayList<Pessoa> getPessoas(Document doc){
		
		ArrayList<Pessoa> array = new ArrayList<Pessoa>();
		
		NodeList nList = doc.getElementsByTagName("pessoa");
		
		for (int i = 0; i < nList.getLength(); i++) {
			Node nNode = nList.item(i);
			
			Element eElement = (Element) nNode;
			Pessoa beam = new Pessoa();
			
			beam.setID(eElement.getElementsByTagName("id").item(0).getTextContent());
			beam.setName(eElement.getElementsByTagName("nome").item(0).getTextContent());
			beam.setDescription(eElement.getElementsByTagName("descricao").item(0).getTextContent());
			beam.setLinkImage(eElement.getElementsByTagName("imagem").item(0).getTextContent());
			
			array.add(beam);
		}
		
		if (array.isEmpty()) {
			return null;
		}else{
			return array;	
		}
	}
	
	public String ListPersona(ArrayList<Pessoa> array){
		String html = 
				
				" <html><head><title>Parse XML</title>"
				+ "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">"
				+ "<link rel=\"stylesheet\" href=\"http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css\">"
				+ "</head><body>"
				
				
				
				+ "<h1>Test PHP - Andr� Bovendorp </h1>"
				+ "<p> Este c�digo est� dispon�vel em https://github.com/boveloco/Utils/tree/master/Parse_XML Todos direitos reservados a Andr� Bovendorp"
				+ "</p>"
				+ "</br>"
				
				+ "<div class= \"container\">"
				+ "<div class= \"table-responsive\">"
				+ "<table class =\"table\""
				+ "<thead>"
				
				+ "<tr>"
				+ "<th>#</th>"
				+ "<th>Nome</th>"
				+ "<th>Imagem</th>"
				+ "</tr>"
				
				+ "</thead>"
				+ "<tbody>";
		
				String temp = "";
				for (int i = 0; i < array.size(); i++) {
					temp +=     "<tr>"
							  + "<td>"+array.get(i).getID()+"</td>"
							  + "<td>"+array.get(i).getName()+"</td>"
							  + "<td><img src=\""+array.get(i).getLinkImage()+"\" height=\"50\" width=\"50\"></td>"
							  + "</tr>";
				}
				html += temp 
				+"</tbody>"
				+ "<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js\"></script>"
				+"<script src=\"http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js\"></script>"
				+ "</table></div></div></body></html>";
		return html;
		
	}
	
	public String ListDetailPersona(ArrayList<Pessoa> array){
String html = 
				
				" <html><head><title>Parse XML</title>"
				+ "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">"
				+ "<link rel=\"stylesheet\" href=\"http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css\">"
				+ "</head><body>"
				
				+ "<h1>Test PHP - Andr� Bovendorp </h1>"
				+ "<p> Este c�digo est� dispon�vel em https://github.com/boveloco/Utils/tree/master/Parse_XML Todos direitos reservados a Andr� Bovendorp"
				+ "</p>"
				+ "</br>"
				
				+ "<div class= \"container\">"
				+ "<div class= \"table-responsive\">"
				+ "<table class= \"table\""
				+ "<thead>"
				
				+ "<tr>"
				+ "<th>#</th>"
				+ "<th>Nome</th>"
				+ "<th>Descricao</th>"
				+ "<th>Imagem</th>"
				+ "</tr>"
				
				+ "</thead>"
				+ "<tbody>";
		
				String temp = "";
				for (int i = 0; i < array.size(); i++) {
					temp +=     "<tr>"
							  + "<td>"+array.get(i).getID()+"</td>"
							  + "<td>"+array.get(i).getName()+"</td>"
							  + "<td>"+array.get(i).getDescription()+"</td>"
							  + "<td><img src=\""+array.get(i).getLinkImage()+"\" height=\"50\" width=\"50\"></td>"
							  + "</tr>";
				}
				html += temp 
				+"</tbody>"
				+ "<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js\"></script>"
				+"<script src=\"http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js\"></script>"
				+ "</table></div></div></body></html>";
				
		return html;
	}
	
}

